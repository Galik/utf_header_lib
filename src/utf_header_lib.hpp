#ifndef UTF_HEADER_LIB_HPP
#define UTF_HEADER_LIB_HPP
/*
 * utf_header_lib.hpp
 *
 *  Created on: 10 Oct 2020
 *      Author: galik
 */

#include <array>
#include <string>

#define char8_t char8_t

namespace utf_header_lib {

namespace detail {

using char16_pair = std::array<char16_t, 2>;

inline constexpr unsigned number_of_bytes(char8_t const c)
{
	constexpr unsigned char const size_table[] =
	{
		  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
		, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
		, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
		, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
		, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
		, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
		, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
		, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
		, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
		, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
		, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
		, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
		, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
		, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
		, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3
		, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 0, 0
	};

	return unsigned(size_table[c]);
}

inline char32_t utf8_to_utf32(char8_t const* cp)
{
	auto sz = number_of_bytes(*cp);

	if(sz == 1)
		return (unsigned char)*cp;

	auto c32 = char32_t((0b0111'1111 >> sz) & (*cp));

	for(unsigned char i = 1; i < sz; ++i)
		c32 = (c32 << 6) | (cp[i] & 0b0011'1111);

	return c32;
}

/**
 *
 * @param cp
 * @param cp16 MUST point to the first element of an array of 2 char16_t elements.
 */
inline unsigned utf8_to_utf16(char8_t const* cp, char16_t* cp16)
{
	char32_t c32 = utf8_to_utf32(cp);

	if(c32 < 0xD800 || (c32 > 0xDFFF && c32 < 0x10000))
	{
		cp16[0] = char16_t(c32);
		cp16[1] = 0;
		return 1;
	}

	c32 -= 0x010000;

	cp16[0] = char16_t(((0b1111'1111'1100'0000'0000 & c32) >> 10) + 0xD800);
	cp16[1] = char16_t(((0b0000'0000'0011'1111'1111 & c32) >> 00) + 0xDC00);

	return 2;
}

/**
 *
 * @param c32
 * @param cp MUST point to the first element of an array of 4 char8_t elements.
 */
inline unsigned utf32_to_utf8(char32_t const c32, char8_t* cp)
{
	if(c32 < 0x0080)
	{
		// 0xxxxxxx
		cp[0] = char8_t(c32);
		return 1;
	}

	if(c32 < 0x0800)
	{
		// 110xxxxx 10xxxxxx
		cp[0] = 0b1100'0000 | c32 >> 6;
		cp[1] = 0b1000'0000 | c32 & 0b0011'1111;
		return 2;
	}


	if(c32 < 0x10000)
	{
		// 1110xxxx 10xxxxxx 10xxxxxx
		cp[0] = 0b1110'0000 | c32 >> 12;
		cp[1] = 0b1000'0000 | (c32 & 0b1111'1100'0000) >> 6;
		cp[2] = 0b1000'0000 | c32 & 0b0011'1111;
		return 3;
	}

	// 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx

	cp[0] = 0b1111'0000 | c32 >> 18;
	cp[1] = 0b1000'0000 | (c32 & 0b11'1111'0000'0000'0000) >> 12;
	cp[1] = 0b1000'0000 | (c32 & 0b1111'1100'0000) >> 6;
	cp[3] = 0b1000'0000 | c32 & 0b0011'1111;

	return 4;
}

}  // namespace detail

template<bool is_a_constant>
class basic_utf8_iterator
{
public:
	using char_type = std::conditional_t<is_a_constant, char8_t const, char8_t*>;
	using pointer = char_type*;
	using const_pointer = std::remove_cv_t<char_type> const*;
	using reference = char_type&;
	using const_reference = std::remove_cv_t<char_type> const&;

	basic_utf8_iterator(char8_t* ptr): m_ptr(ptr) {}

	bool operator==(basic_utf8_iterator<true> const& other) const { return other.m_ptr == m_ptr; }
	bool operator!=(basic_utf8_iterator<true> const& other) const { return !(*this == other); }

	basic_utf8_iterator& operator++()    { m_ptr += detail::number_of_bytes(*m_ptr); return *this; }
	basic_utf8_iterator  operator++(int) { auto copy = *this; ++(*this); return copy; }

	reference       operator*()       { return *m_ptr; }
	const_reference operator*() const { return *m_ptr; }

	pointer       operator->()       { return m_ptr; }
	const_pointer operator->() const { return m_ptr; }

private:
	pointer m_ptr = nullptr;
};

inline
std::u32string utf8_to_utf32(std::u8string const& utf8)
{
	std::u32string utf32;

	for(unsigned i = 0; i != utf8.size(); i += detail::number_of_bytes(utf8[i]))
		utf32 += detail::utf8_to_utf32(&utf8[i]);

	return utf32;
}

inline
std::u8string utf32_to_utf8(std::u32string const& utf32)
{
	std::u8string utf8;

	char8_t cp[4];

	for(auto const c32: utf32)
	{
		auto n = detail::utf32_to_utf8(c32, cp);
		utf8.append(cp, n);
	}

	return utf8;
}

}  // namespace utf_header_lib

#endif // UTF_HEADER_LIB_HPP
