
SRC=src
BIN=bin

CXX := g++ -std=c++2a -fchar8_t

all: test

test: $(SRC)/test.cpp $(SRC)/utf_header_lib.hpp
	$(CXX) -pedantic-errors -o $@ $<

clean: 
	$(RM) test

.PHONY: all clean